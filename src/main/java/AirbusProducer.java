

import com.myntra.airbus.entry.EventEntry;
import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.producer.Producer;
import com.myntra.airbus.producer.impl.ProducerImpl;
import com.myntra.airbus.shaded.springframework.transaction.support.TransactionSynchronizationAdapter;
import com.myntra.airbus.shaded.springframework.transaction.support.TransactionSynchronizationManager;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by 300068559 on 18/03/20.
 */

public class AirbusProducer {

    private String application;
    private Producer producer;

   public AirbusProducer() {

        String serviceUrl = "http://airbus.myntra.com";
        String application = "rashi";
        this.application = application;
        try {
            producer = new ProducerImpl(serviceUrl, application);
        } catch (ManagerException e) {
            System.out.print("Error in creating Producer" + e);
        }
    }


    public void publishToAirbus(String eventName, String data) {

        EventEntry eventEntry = new EventEntry();
        eventEntry.setAppName(application);
        eventEntry.setEventName(eventName);
        eventEntry.setData(data);

        try {
            if (TransactionSynchronizationManager.isSynchronizationActive()) {
                TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                    @Override
                    public void afterCommit() {
                        try {
                            producer.send(eventEntry);
                        }catch (ManagerException e) {
                            System.out.print("Airbus Issue "+ e);
                        }
                    }
                });
            } else {
                producer.send(eventEntry);
            }
        }catch (ManagerException e) {
            System.out.print("Airbus Issue "+ e);
        }

    }
}
