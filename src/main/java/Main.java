import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by 300068559 on 23/03/20.
 */
public class Main {
    public static void main(String args[]) {
        System.out.print("*********** enter 1. profile   2. email    3. phone ");

        Scanner sc= new Scanner(System.in);
        int a= sc.nextInt();
        AirbusProducer airbusProducer = new AirbusProducer();
        ExecutorService executor = Executors.newFixedThreadPool(10);

        if(a == 1) {

            for (int i = 0; i <= 3420; i++) {
                Long start = i * 50000L;
                Long last = (i + 1) * 50000L - 1;
                Runnable runnable = new MyRunnable(start, last, airbusProducer);
                executor.execute(runnable);
            }

        }else  if(a == 2) {
            for (int i = 0; i <= 3420; i++) {

                Long start = i * 50000L;
                Long last = (i + 1) * 50000L - 1;
                Runnable runnable = new MyRunnable1(start, last, airbusProducer);
                executor.execute(runnable);
            }
        }
        else if(a == 3) {
            for (int i = 0; i <= 2099; i++) {
                Long start = i * 50000L;
                Long last = (i + 1) * 50000L - 1;
                Runnable runnable = new MyRunnable2(start, last, airbusProducer);
                executor.execute(runnable);
            }
        }
        executor.shutdown();
    }
}
