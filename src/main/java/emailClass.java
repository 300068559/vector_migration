import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by 300068559 on 17/03/20.
 */

class MyRunnable1 implements Runnable{
    AirbusProducer airbusProducer;
    Long start, last = 0L;


    MyRunnable1(Long start, Long last, AirbusProducer airbusProducer) {
        this.start = start;
        this.last = last;
        this.airbusProducer = airbusProducer;
    }

    class vectorList {
        String uid;
        String action;
        ArrayList<vectors> updateVectors;

        public ArrayList<vectors> getUpdateVectors() {
            return updateVectors;
        }

        public void setUpdateVectors(ArrayList<vectors> updateVectors) {
            this.updateVectors = updateVectors;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }



    }

    class vectors {
        String name;
        ArrayList<dimension> dimensions;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<dimension> getDimensions() {
            return dimensions;
        }

        public void setDimensions(ArrayList<dimension> dimensions) {
            this.dimensions = dimensions;
        }

    }

    class dimension {
        Object key;
        Object value;

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getKey() {
            return key;
        }

        public void setKey(Object key) {
            this.key = key;
        }

    }


    void convertToProducerObject(int uid, String uidx, String email, String primary, String verified) {

        ArrayList<vectorList> vectorList = new ArrayList<>();
        vectorList vectorList1 = new vectorList();
        vectorList1.uid  = uidx;
        vectorList1.action = "UPSERT";
        ArrayList<vectors> vectors = new ArrayList<>();
        vectors vector1 = new vectors();
        vector1.name = "profile";

        ArrayList<dimension> dimensions = new ArrayList<>();
        dimension dimension1 = new dimension();
        dimension1.key = "emailDetails";
        Map<String, String> emailDetails = new HashMap<>();
        emailDetails.put("email", email);
        emailDetails.put("primary", primary);
        emailDetails.put("verified", verified);

        dimension1.value = emailDetails;
        dimensions.add(dimension1);

        vector1.dimensions = dimensions;
        vectors.add(vector1);
        vectorList1.updateVectors = vectors;

        vectorList.add(vectorList1);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String value = mapper.writeValueAsString(vectorList);
            FileWriter fileWriter = new FileWriter("success.txt", true);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(value);
            printWriter.close();
            System.out.println(uid + "  " + value);

            produceEventToVector(value);
        }catch (Exception e) {
            System.out.println("Issue facing while mapping object" + e);
        }
    }

    public void produceEventToVector(String value) {

        String eventName = "EXTRAVECTORS";
        airbusProducer.publishToAirbus(eventName, value);
    }

    @Override
    public void run() {
        try
        {
            String myUrl = "jdbc:mysql://ideadbslave1.myntra.com:3306/idea?autoReconnect=true&useSSL=false&READONLY=true";
            Connection conn = DriverManager.getConnection(myUrl, "IdeaAppUser", "wR6nuba2a!em");

            String query = "select DISTINCT(ue.uid), uel.uidx, ue.email, ue.is_primary, ue.verified from user_emails ue " +
                    "left join `user_email_lookup` uel on ue.uid = uel.uid where ue.is_primary = \"1\" and ue.deleted = \"0\" and ue.uid between " + start + " " + "and" + " " + last + ";";
            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery(query);


            while (rs.next())
            {
                int uid = rs.getInt("ue.uid");
                String uidx = rs.getString("uel.uidx");
                String email = rs.getString("ue.email");
                String isPrimary = rs.getString("ue.is_primary").equals("1") ? "true" : "false";
                String isVerified = rs.getString("ue.verified").equals("1") ? "true" : "false";

                convertToProducerObject(uid, uidx, email, isPrimary, isVerified);
            }
            st.close();
            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

    }
}


