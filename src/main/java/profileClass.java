import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by 300068559 on 17/03/20.
 */



class MyRunnable implements Runnable{

    Long start, last = 0L;

    AirbusProducer airbusProducer;

    MyRunnable(Long start, Long last, AirbusProducer airbusProducer) {
        this.start = start;
        this.last = last;
        this.airbusProducer = airbusProducer;
    }

    class vectorList {
        String uid;
        String action;
        ArrayList<vectors> updateVectors;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public ArrayList<vectors> getUpdateVectors() {
            return updateVectors;
        }

        public void setUpdateVectors(ArrayList<vectors> updateVectors) {
            this.updateVectors = updateVectors;
        }


    }

    class vectors {
        String name;
        ArrayList<dimension> dimensions;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<dimension> getDimensions() {
            return dimensions;
        }

        public void setDimensions(ArrayList<dimension> dimensions) {
            this.dimensions = dimensions;
        }

    }

    class dimension {
        Object key;
        Object value;

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getKey() {
            return key;
        }

        public void setKey(Object key) {
            this.key = key;
        }

    }


    void convertToProducerObject(int id, String uidx, String firstName, String lastName, String gender, String reg_time, String status) {

        ArrayList<vectorList> vectorList = new ArrayList<>();
        vectorList vectorList1 = new vectorList();
        vectorList1.uid  = uidx;
        vectorList1.action = "UPSERT";
        ArrayList<vectors> vectors = new ArrayList<>();
        vectors vector1 = new vectors();
        vector1.name = "profile";
        ArrayList<dimension> dimensions = new ArrayList<>();

        if(firstName != null) {
            dimension dimension1 = new dimension();
            dimension1.key = "firstName";
            dimension1.value = firstName;
            dimensions.add(dimension1);
        }

        if(lastName != null) {
            dimension dimension2 = new dimension();
            dimension2.key = "lastName";
            dimension2.value = lastName;
            dimensions.add(dimension2);
        }

        if(gender != null) {
            dimension dimension3 = new dimension();
            dimension3.key = "gender";
            dimension3.value = gender;
            dimensions.add(dimension3);
        }
        if(reg_time != null) {
            dimension dimension4 = new dimension();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            try {
                Date date = df.parse(reg_time);
                long epoch = date.getTime();
                dimension4.key = "registrationOn";
                dimension4.value = epoch;
                dimensions.add(dimension4);

            }catch (Exception e) {
                System.out.print(e);
            }
        }

        if(status != null) {
            dimension dimension5 = new dimension();
            dimension5.key = "status";
            dimension5.value = status;
            dimensions.add(dimension5);
        }

        vector1.dimensions = dimensions;
        vectors.add(vector1);
        vectorList1.updateVectors = vectors;

        vectorList.add(vectorList1);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String value = mapper.writeValueAsString(vectorList);
            System.out.println(id);
            produceEventToVector(value);
        }catch (Exception e) {
            System.out.print("exception ********** " + e);

        }
    }


    public void produceEventToVector(String value) {
        String eventName = "EXTRAVECTORS";
        airbusProducer.publishToAirbus(eventName, value);
    }

    @Override
    public void run() {
        try
        {
            String myUrl = "jdbc:mysql://ideadbslave1.myntra.com:3306/idea?autoReconnect=true&useSSL=false&READONLY=true";
            Connection conn = DriverManager.getConnection(myUrl, "IdeaAppUser", "wR6nuba2a!em");

            String query = "SELECT DISTINCT(u.id), u.uidx, u.registration_on, u.first_name, u.last_name, u.gender, us.status FROM user u left join " +
                    "user_client_status us on u.id = us.uid where u.id between " + start + " " + "and" + " " + last + ";";
            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery(query);

            while (rs.next())
            {
                int id = rs.getInt("u.id");
                String uidx = rs.getString("u.uidx");
                String fname = rs.getString("u.first_name");
                String lname = rs.getString("u.last_name");
                String reg_time = rs.getString("u.registration_on");
                String status;
                String gender;
                switch (rs.getString("u.gender")) {
                    case "0": gender = "NOTSET"; break;
                    case "1": gender = "MALE"; break;
                    case "2": gender = "FEMALE"; break;
                    default: gender = "NOTSET";
                }
                if(rs.getString("us.status") != null) {
                    switch (rs.getString("us.status")) {
                        case "0":
                            status = "INACTIVE";
                            break;
                        case "1":
                            status = "ACTIVE";
                            break;
                        case "2":
                            status = "TEMPORARY_BLOCKED";
                            break;
                        case "3":
                            status = "ACCOUNT_SUSPENDED";
                            break;
                        case "4":
                            status = "BLACKLISTED";
                            break;
                        case "5":
                            status = "INACTIVE_LOCKED";
                            break;
                        case "6":
                            status = "REPORTED";
                            break;
                        default:
                            status = "ACTIVE";
                    }
                }else {
                    status = "ACTIVE";
                }
                convertToProducerObject(id, uidx, fname, lname, gender, reg_time, status);
            }
            st.close();
            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

    }
}

