/**
 * Created by 300068559 on 23/03/20.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by 300068559 on 17/03/20.
 */

class MyRunnable2 implements Runnable{

            AirbusProducer airbusProducer;
    Long start, last = 0L;

    MyRunnable2(Long start, Long last, AirbusProducer airbusProducer) {
        this.start = start;
        this.last = last;
        this.airbusProducer = airbusProducer;
    }

    class vectorList {
        String uid;
        String action;
        ArrayList<vectors> updateVectors;

        public ArrayList<vectors> getUpdateVectors() {
            return updateVectors;
        }

        public void setUpdateVectors(ArrayList<vectors> updateVectors) {
            this.updateVectors = updateVectors;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

    }

    class vectors {
        String name;
        ArrayList<dimension> dimensions;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<dimension> getDimensions() {
            return dimensions;
        }

        public void setDimensions(ArrayList<dimension> dimensions) {
            this.dimensions = dimensions;
        }

    }

    class dimension {
        Object key;
        Object value;

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Object getKey() {
            return key;
        }

        public void setKey(Object key) {
            this.key = key;
        }

    }


    void convertToProducerObject(int uid, String uidx, String email, String primary, String verified) {

        ArrayList<vectorList> vectorList = new ArrayList<>();
        vectorList vectorList1 = new vectorList();
        vectorList1.uid  = uidx;
        vectorList1.action = "UPSERT";
        ArrayList<vectors> updateVectors = new ArrayList<>();
        vectors vector1 = new vectors();
        vector1.name = "profile";

        ArrayList<dimension> dimensions = new ArrayList<>();
        dimension dimension1 = new dimension();
        dimension1.key = "phoneDetails";
        Map<String, String> emailDetails = new HashMap<>();
        emailDetails.put("phone", email);
        emailDetails.put("primary", primary);
        emailDetails.put("verified", verified);

        dimension1.value = emailDetails;
        dimensions.add(dimension1);

        vector1.dimensions = dimensions;
        updateVectors.add(vector1);
        vectorList1.updateVectors = updateVectors;

        vectorList.add(vectorList1);

        ObjectMapper mapper = new ObjectMapper();

        try {
            String value = mapper.writeValueAsString(vectorList);
            System.out.println(uid + " " + value);
            produceEventToVector(value);

        }catch (Exception e) {
            System.out.println("Issue facing while mapping object" + e);
        }
    }

    public void produceEventToVector(String value) {

        String eventName = "EXTRAVECTORS";
        airbusProducer.publishToAirbus(eventName, value);
    }

    @Override
    public void run() {
        try
        {
            String myUrl = "jdbc:mysql://ideadbslave1.myntra.com:3306/idea?autoReconnect=true&useSSL=false&READONLY=true";
            Connection conn = DriverManager.getConnection(myUrl, "IdeaAppUser", "wR6nuba2a!em");

            String query = "select Distinct(uid), uidx, phone, verified, deleted from user_phone_lookup where id between " + start + " " + "and" + " " + last +
                    " and deleted = \"0\" ;";
            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery(query);


            while (rs.next())
            {
                int uid = rs.getInt("uid");
                String uidx = rs.getString("uidx");
                String phone = rs.getString("phone");
                String isPrimary = rs.getString("deleted").equals("1") ? "false" : "true";
                String isVerified = rs.getString("verified").equals("1") ? "true" : "false";

                convertToProducerObject(uid, uidx, phone, isPrimary, isVerified);
            }
            st.close();
            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

    }
}
